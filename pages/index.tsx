import type { NextPage } from 'next'
// import HomePage from './home'
import DragDropPage from './dragDrop'

const Home: NextPage = () => {
  return <DragDropPage />
}

export default Home
