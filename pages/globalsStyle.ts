import { createGlobalStyle } from 'styled-components'
import { IThemeProps } from 'src/utils/theme'

export const topBarHeight = 46
export const bottomBarHeight = 60

export default createGlobalStyle<IThemeProps>`
:root {
  font-family: sans-serif;
  text-align: center;
  --topBarHeight: ${topBarHeight}px;
  --bottomBarHeight: calc(env(safe-area-inset-bottom) + ${bottomBarHeight}px);
  background: ${(p) => p.theme.bg};
}

body, html {
  // overflow: hidden;
  user-select: none;
  background: #03a678;
  color: #333;
  -webkit-tap-highlight-color: transparent;
}

* {
  font-family: Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  -webkit-overflow-scrolling: touch;
  &::-webkit-scrollbar { width: 0; height: 0; }
  margin: 0;
}

#root {
  position: relative;
  width: 100vw;
  height: var(--100vh);
  overflow: hidden;
}

input, select {
  appearance: none;
}
`
