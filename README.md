## React + react-beautiful-dnd + styled-components

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
___

## react-beautiful-dnd 主要 3 個主要元件
### `DragDropContext` - 建立一個可 DnD 的範圍
- onDragStart
- onDragUpdate
- onDragEnd
### `Droppable` - 建立可以被拖曳放入的區域
### `Draggalbe` - 可被拖拉卡片

```bash
yarn add react-beautiful-dnd
```
### 步驟 一: 
- 要 DnD 的位置使用 <DragDropContext></DragDropContext> 包起來，且 onDragEnd 必須設定
### 步驟 二: 
- 使用 <Droppable></Droppable> 配置可被拖入的卡片
- 需加入 {provided.placeholder}
### 步驟 三:
- 將要拖移的卡片使用 <Draggable></Draggable> 包起來
