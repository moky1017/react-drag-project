export interface ICalendarState {
  loading: boolean
  myData: Array<IMyDataProps>
}

export interface IMyDataProps {
  id: number | null
  startDate: string | null
  endDate: string | null
  title: string
  detail: string
}
