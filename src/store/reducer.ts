import { combineReducers } from 'redux'
import { AnyAction } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import calendarSlice from './slice/calendarSlice'
import tasksSlice from './slice/tasksSlice'

const combinedReducer = combineReducers({
  calendarSlice,
  tasksSlice, // [counterSlice.name]: counterSlice
})

const reducer = (state: ReturnType<typeof combinedReducer>, action: AnyAction) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    }
    return nextState
  } else {
    return combinedReducer(state, action)
  }
}

export default reducer
