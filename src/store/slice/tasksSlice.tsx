import { createSlice, current } from '@reduxjs/toolkit'
import type { AppState } from '../store'

const initialState: any = {
  loading: false,
  taskData: [
    { id: 'task-1', content: 'Take out the garbage' },
    { id: 'task-2', content: 'Watch my favorite show' },
    { id: 'task-3', content: 'Charge my phone' },
    { id: 'task-4', content: 'Cook dinner' },
  ],
  columnData: [
    {
      id: 'column-1',
      title: 'To do',
      taskIds: ['task-1', 'task-2', 'task-3'],
    },
    {
      id: 'column-2',
      title: 'In Progress',
      taskIds: ['task-4'],
    },
    {
      id: 'column-3',
      title: 'Done',
      taskIds: [],
    },
  ],
}

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    loadDataAction: (state, { payload }) => {
      const { columns, tasks } = payload
      return {
        ...state,
        taskData: tasks,
        columnData: columns,
      }
    },
    addTaskItemAction: (state, { payload }) => {
      const { columnId, form } = payload
      const columnDataFilter = current(state?.columnData).map((item: any, index: number) => {
        if (item.id !== columnId) return item
        const columnItem = [...item.taskIds, form.id]
        return { ...item, taskIds: columnItem }
      })

      return {
        ...state,
        taskData: [...state.taskData, form],
        columnData: columnDataFilter,
      }
    },
    deleteTaskItemAction: (state, { payload }) => {
      const taskData = current(state.taskData).filter(
        (item: any, index: number) => item.id !== payload.taskId,
      )
      const columnData = current(state.columnData).map((item: any, index: number) => {
        if (item.id === payload.columnId) {
          const sel = item.taskIds.filter((s: any, index: number) => s !== payload.taskId)
          return { ...item, taskIds: sel }
        } else return item
      })

      return {
        ...state,
        taskData: taskData,
        columnData: columnData,
      }
    },
    editTaskItemAction: (state, { payload }) => {
      const data = current(state.taskData).map((item: any, index: number) => {
        if (item.id !== payload.id) return item
        return payload
      })

      return {
        ...state,
        taskData: data,
      }
    },
  },
})

export const tasksReducer = (state: AppState) => state
export const { loadDataAction, addTaskItemAction, deleteTaskItemAction, editTaskItemAction } =
  tasksSlice.actions
export default tasksSlice.reducer
