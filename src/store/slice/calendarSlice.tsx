import { createSlice, current } from '@reduxjs/toolkit'
import type { AppState } from '../store'
import { ICalendarState, IMyDataProps } from 'src/interface/calendarType'

const initialState: ICalendarState = {
  loading: false,
  myData: [],
}

export const calendarSlice = createSlice({
  name: 'calendar',
  initialState,
  reducers: {
    addDateAction: (state, action) => {
      return {
        ...state,
        myData: [...state.myData, action.payload],
      }
    },

    deleteAction: (state, action) => {
      // current 取得當前state
      // const data = (current(state.myData)).filter((item: any, index: any) => index !== action.payload)
      return {
        ...state,
        myData: state.myData.filter(
          (item: IMyDataProps, index: number) => action.payload !== item.id,
        ),
      }
    },

    editAction: (state, action) => {
      const data = state.myData.map((item: IMyDataProps) => {
        if (item.id !== action.payload.id) return item
        return { ...item, detail: action.payload.detail }
      })
      return {
        ...state,
        myData: data,
      }
    },
  },
})

export const calendarReducer = (state: AppState) => state
export const { addDateAction, deleteAction, editAction } = calendarSlice.actions
export default calendarSlice.reducer
