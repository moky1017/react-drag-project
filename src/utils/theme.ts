interface IDefaultTheme {
  bg: string
  border: string
  borderFocus: string
  placeholder: string
  text: string
  dateText: string
}

export const theme: IDefaultTheme = {
  bg: '#fff',
  border: 'rgba(25, 118, 210, 0.5)',
  borderFocus: '#1976d2',
  placeholder: 'rgba(25, 118, 210, 0.5)',
  text: '#0074be',
  dateText: '#c1c1c1',
}
export type IThemeProps = {
  theme: typeof theme
}
