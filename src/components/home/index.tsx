import React from 'react'
import DateSettingModel from './dateSetting/index'
import ContentModel from './content/index'
import styled from 'styled-components'

const HomeContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${(p) => p.theme.bg};
`

const HomePageModel = () => {
  return (
    <HomeContainer>
      <DateSettingModel />
      <ContentModel />
    </HomeContainer>
  )
}

export default HomePageModel
