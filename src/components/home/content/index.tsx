import React from 'react'
import styled from 'styled-components'
import { deleteAction, calendarReducer } from 'src/store/slice/calendarSlice'
import { useAppDispatch, useAppSelector } from 'src/utils/hooks/reduxHook'
import CardModel from './cardModel'
import { IMyDataProps } from 'src/interface/calendarType'

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow-y: scroll;
`

const ContentModel = () => {
  const dispatch = useAppDispatch()
  const { calendarSlice } = useAppSelector(calendarReducer)

  return (
    <Container>
      {calendarSlice?.myData.length > 0 &&
        calendarSlice?.myData.map((data: IMyDataProps, index: number) => (
          <CardModel key={index} data={data} onClick={() => dispatch(deleteAction(data.id))} />
        ))}
    </Container>
  )
}

export default ContentModel
