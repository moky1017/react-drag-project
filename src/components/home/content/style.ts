import styled from 'styled-components'
import { Button } from '@mui/material'

export const CardContainer = styled.div`
  margin: 10px;
  padding: 10px;
  width:calc(150px * 3 + 20px);
  min-width: 356px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start
  color: ${(p) => p.theme.text};
  border: 1px solid ${(p) => p.theme.border};
  border-radius: 10px;
  // box-shadow: 0 2px 5px #b2c5fb;
`

export const TextContent = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 10px;
  color: ${(p) => p.theme.text};
`

export const TitleText = styled.div`
  font-size: 14px;
  color: ${(p) => p.theme.text};
`

export const DateText = styled.div`
  font-size: 12px;
  color: ${(p) => p.theme.dateText};
  margin-left: 5px;
`

export const EditTextarea = styled.textarea.attrs((p) => ({
  placeholder: 'Detail',
  rows: '4',
  value: p.value,
}))`
  resize: none;
  width: calc(150px * 3 + 20px);
  background: #fff;
  font-size: 14px;
  border: 1px solid ${(p) => p.theme.border};
  color: ${(p) => p.theme.text};
  outline: none;
  border-radius: 5px;
  padding: 10px;
  &:focus {
    border: 1px solid ${(p) => p.theme.borderFocus};
  }
  &::placeholder {
    color: ${(p) => p.theme.placeholder};
  }
`

export const ButtonList = styled.div`
  display: flex;
  align-self: flex-end;
`

export const ButtonItem = styled(Button)`
  margin: 5px;
  font-size: 12px;
`
