import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from 'src/utils/hooks/reduxHook'
import moment from 'moment'
import { addDateAction, calendarReducer } from 'src/store/slice/calendarSlice'
import { Fab } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import {
  DateSettingContainer,
  BasicDateContainer,
  DatePickerContainer,
  DateIcon,
  DateButton,
  TitleInput,
  DetailTextarea,
} from './style'

interface IFormProps {
  id: number | null
  startDate: Date | null
  endDate: Date | null
  title: string
  detail: string
}

const form: IFormProps = {
  id: null,
  startDate: null,
  endDate: null,
  title: '',
  detail: '',
}

const DateSettingModel = () => {
  const [formData, setFormDate] = useState(form)
  const dispatch = useAppDispatch()
  const { calendarSlice } = useAppSelector(calendarReducer)

  const handleOnClick = () => {
    const start = moment(formData.startDate).format('YYYY/MM/DD').toString()
    const end = moment(formData.endDate).format('YYYY/MM/DD').toString()
    if (!start || !end || !formData?.title || !formData.detail) return
    setFormDate(form)
    dispatch(
      addDateAction({
        ...formData,
        startDate: start,
        endDate: end,
        id: calendarSlice?.myData.length,
      }),
    )
  }

  const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
    setFormDate({ ...formData, detail: e.target.value })
  }

  return (
    <DateSettingContainer>
      <BasicDateContainer>
        <TitleInput
          value={formData.title}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setFormDate({ ...formData, title: e.target.value })
          }
        />
        <DatePickerContainer
          // dateFormat="yyyy/MM/dd"
          placeholderText="Select a weekday"
          todayButton="Vandaag"
          selected={formData.startDate}
          onChange={(date: Date) => setFormDate({ ...formData, startDate: date })}
          selectsStart
          startDate={formData.startDate}
          endDate={formData.endDate}
          customInput={
            <DateButton>
              <DateIcon />
              {formData.startDate
                ? moment(formData.startDate).format('YYYY/MM/DD').toString()
                : 'start date'}
            </DateButton>
          }
        />
        <DatePickerContainer
          //  dateFormat="yyyy/MM/dd"
          selected={formData.endDate}
          todayButton="Vandaag"
          onChange={(date: Date) => setFormDate({ ...formData, endDate: date })}
          selectsEnd
          startDate={formData.startDate}
          endDate={formData.endDate}
          minDate={formData.startDate}
          customInput={
            <DateButton>
              <DateIcon />
              {formData.endDate
                ? moment(formData.endDate).format('YYYY/MM/DD').toString()
                : 'end date'}
            </DateButton>
          }
        />
      </BasicDateContainer>

      <DetailTextarea value={formData.detail} onChange={handleOnChange} />

      <Fab
        size="small"
        color="primary"
        aria-label="add"
        onClick={handleOnClick}
        sx={{ position: 'absolute', bottom: '20px', right: 0 }}
      >
        <AddIcon />
      </Fab>
    </DateSettingContainer>
  )
}

export default DateSettingModel
