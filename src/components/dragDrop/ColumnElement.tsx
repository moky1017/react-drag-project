import { Droppable, Draggable } from 'react-beautiful-dnd'
import ListItem from './ListItem'
import React from 'react'
import styled from 'styled-components'
import { IColumn, ITask } from './type'
import { tasksReducer, addTaskItemAction } from 'src/store/slice/tasksSlice'
import { useAppDispatch, useAppSelector } from 'src/utils/hooks/reduxHook'

const ColumnElementContainer = styled.div`
  padding: 10px;
  border-radius: 6px;
  background: #d4d4d4;
  display: flex;
  justify-content: center;
`

const Container = styled.div`
  margin: 8px;
  border: 1px solid lightgrey;
  border-radius: 2px;
  width: 220px;
  display: flex;
  flex-direction: column;
  min-height: 300px;
  width: 80%;
`

const Title = styled.h3`
  padding: 8px;
`

const TaskList = styled.div`
  padding: 8px;
  flex-grow: 1;
  min-height: 100px;
`

type Props = {
  column: IColumn
  tasks: ITask[]
  index: number
}

const ColumnElement: React.FC<Props> = ({ column, tasks, index }) => {
  const { tasksSlice } = useAppSelector(tasksReducer)
  const { taskData } = tasksSlice
  const dispatch = useAppDispatch()

  const columTasks = column.taskIds.map((curId) => {
    const task = tasks.filter((task) => task.id === curId)
    return task[0]
  })

  const handleOnClick = (id: string) => {
    const form = { id: `task-${taskData?.length + 1}`, content: `task-${taskData?.length + 1}` }
    const isId = tasksSlice.taskData.some((item: any, index: number) => item.id === form.id)
    if (!isId) dispatch(addTaskItemAction({ columnId: id, form }))
  }

  // Draggable 中一定要加 {provided.placeholder} 不然會報錯
  return (
    <ColumnElementContainer>
      <Draggable draggableId={column.id} index={index}>
        {(provided) => (
          <Container {...provided.draggableProps} ref={provided.innerRef}>
            <Title {...provided.dragHandleProps}>{column.title}</Title>
            <Droppable droppableId={column.id} type="task">
              {(provided) => (
                <TaskList ref={provided.innerRef} {...provided.droppableProps}>
                  {columTasks.map((colTask, index) => (
                    <ListItem key={colTask.id} task={colTask} index={index} columnId={column.id} />
                  ))}
                  {provided.placeholder}
                </TaskList>
              )}
            </Droppable>
            <button onClick={() => handleOnClick(column.id)}>add</button>
          </Container>
        )}
      </Draggable>
    </ColumnElementContainer>
  )
}

export default ColumnElement
