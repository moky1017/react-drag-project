import { ITask, IColumn } from './type'

export const taskData: ITask[] = [
  { id: 'task-1', content: 'Take out the garbage' },
  { id: 'task-2', content: 'Watch my favorite show' },
  { id: 'task-3', content: 'Charge my phone' },
  { id: 'task-4', content: 'Cook dinner' },
]

export const columnData: IColumn[] = [
  {
    id: 'column-1',
    title: 'To do',
    taskIds: ['task-1', 'task-2', 'task-3'],
  },
  {
    id: 'column-2',
    title: 'In Progress',
    taskIds: ['task-4'],
  },
  {
    id: 'column-3',
    title: 'Done',
    taskIds: [],
  },
]
