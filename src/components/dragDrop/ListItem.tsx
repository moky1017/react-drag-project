import { Draggable } from 'react-beautiful-dnd'
import React, { useState } from 'react'
import { ITask } from './type'
import styled from 'styled-components'
import { useAppDispatch } from 'src/utils/hooks/reduxHook'
import { deleteTaskItemAction, editTaskItemAction } from 'src/store/slice/tasksSlice'
import { Button, Box } from '@mui/material'

const CardFooter = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const DragItem = styled.div`
  width: 100%;
  padding: 10px;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  background: white;
  margin: 0 0 8px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const ContentTextarea = styled.textarea.attrs((p) => ({
  value: p.value,
}))`
  color: black;
  outline: none;
  width: 100%;
  resize: none;
  border: 1px solid ${(p) => p.theme.border};
  background: #fff;
  padding: 5px;
`

type Props = {
  task: ITask
  index: number
  columnId: string
}

const ListItem: React.FC<Props> = ({ task, index, columnId }) => {
  const [isEdit, setEdit] = useState(false)
  const [taskFrom, setTaskData] = useState(task)

  const dispatch = useAppDispatch()

  const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    // e:React.ChangeEvent<HTMLInputElement>
    const { value } = e.target
    console.log()
    setTaskData({ ...taskFrom, content: value })
  }

  const handleEditOnClick = () => {
    setEdit(false)
    dispatch(editTaskItemAction(taskFrom))
  }

  const handleDeleteTask = () => {
    dispatch(deleteTaskItemAction({ columnId, taskId: taskFrom.id }))
  }

  return (
    <Draggable draggableId={taskFrom.id} index={index}>
      {(provided) => {
        return (
          <DragItem
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            {isEdit ? (
              <ContentTextarea value={taskFrom.content} onChange={(e) => handleOnChange(e)} />
            ) : (
              <CardFooter>{taskFrom.content}</CardFooter>
            )}

            <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
              {isEdit ? (
                <Button variant="outlined" sx={{ m: 1 }} onClick={handleEditOnClick}>
                  Summit
                </Button>
              ) : (
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => setEdit(true)}>
                  Edit
                </Button>
              )}

              <Button variant="outlined" sx={{ m: 1 }} onClick={handleDeleteTask}>
                Delete
              </Button>
            </Box>
          </DragItem>
        )
      }}
    </Draggable>
  )
}

export default ListItem
