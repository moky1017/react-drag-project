export type ITask = {
  id: string
  content: string
}

export type IColumn = {
  id: string
  title: string
  taskIds: string[]
}
