import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { DragDropContext, Droppable, DropResult, DragStart, DragUpdate } from 'react-beautiful-dnd'
import { ITask, IColumn } from './type'
import ColumnElement from './ColumnElement'
import { useAppDispatch, useAppSelector } from 'src/utils/hooks/reduxHook'
import { tasksReducer, loadDataAction } from 'src/store/slice/tasksSlice'
// import dynamic from 'next/dynamic'
// const ColumnElement = dynamic(import('./ColumnElement'), { ssr: false })

const DragDropContextContainer = styled.div`
  padding: 20px;
  border: 4px solid indianred;
  border-radius: 6px;
`

const ListGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 8px;
`

const getDraggedElm = (draggableId: string) => {
  // Returns the element being dragged
  const queryAttr = 'data-rbd-drag-handle-draggable-id'
  const domQuery = `[${queryAttr}='${draggableId}']`
  return document.querySelector(domQuery)
}

const DragListModel = () => {
  const { tasksSlice } = useAppSelector(tasksReducer)
  const [tasks, setTasks] = useState<ITask[]>(tasksSlice?.taskData)
  const [columns, setColumns] = useState<IColumn[]>(tasksSlice?.columnData)
  const [winReady, setWinReady] = useState(false)

  const dispatch = useAppDispatch()

  useEffect(() => {
    setWinReady(true)
  }, [])

  useEffect(() => {
    setTasks(tasksSlice?.taskData)
  }, [tasksSlice?.taskData])

  useEffect(() => {
    setColumns(tasksSlice?.columnData)
  }, [tasksSlice?.columnData])

  useEffect(() => {
    dispatch(loadDataAction({ columns, tasks }))
  }, [columns, tasks])

  const onDragStart = (initial: DragStart) => {
    const draggedElm = getDraggedElm(initial.draggableId)
    if (!draggedElm) return
  }

  const onDragUpdate = (initial: DragUpdate) => {
    if (!initial.destination) return
    const draggedElm = getDraggedElm(initial.draggableId)
    if (!draggedElm) return
  }

  const onDragEnd = (result: DropResult) => {
    const { destination, source, draggableId, type } = result

    if (!destination) return
    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return
    }

    const startColId = source.droppableId
    const endColId = destination.droppableId

    if (startColId === endColId) {
      const column = columns.filter((column) => column.id === source.droppableId)[0]
      const newTaskIds = [...column.taskIds]

      // remove the selected item from the newTaskIds array
      newTaskIds.splice(source.index, 1)

      // add the selected item where it is placed
      newTaskIds.splice(destination.index, 0, draggableId)

      const newColumn = {
        ...column,
        taskIds: newTaskIds,
      }

      setColumns((prev) => {
        const newColumnIndex = prev.findIndex((column) => column.id === newColumn.id)
        const newColumns = [...prev]
        newColumns[newColumnIndex] = newColumn
        return newColumns
      })
    } else {
      // Remove task from start column
      const startCol = columns.filter((column) => column.id === source.droppableId)[0]
      const updatedStartTaskIds = [
        ...startCol.taskIds.slice(0, source.index),
        ...startCol.taskIds.slice(source.index + 1),
      ]

      const updatedStartCol = {
        ...startCol,
        taskIds: updatedStartTaskIds,
      }
      // Add Task to position in end column
      const endCol = columns.filter((column) => column.id === destination.droppableId)[0]

      const updatedEndTaskIds = [
        ...endCol.taskIds.slice(0, destination.index),
        draggableId,
        ...endCol.taskIds.slice(destination.index),
      ]

      const updatedEndCol = {
        ...endCol,
        taskIds: updatedEndTaskIds,
      }

      setColumns((prev) => {
        const startColIdx = prev.findIndex((column) => column.id === startCol.id)
        const endColIdx = prev.findIndex((column) => column.id === endCol.id)
        const newCols = [...prev]
        newCols[startColIdx] = updatedStartCol
        newCols[endColIdx] = updatedEndCol

        return newCols
      })
    }
  }

  return (
    <DragDropContextContainer>
      <DragDropContext onDragStart={onDragStart} onDragUpdate={onDragUpdate} onDragEnd={onDragEnd}>
        {winReady && (
          <Droppable droppableId="all-columns-1" direction="horizontal" type="column">
            {(provided) => (
              <ListGrid {...provided.droppableProps} ref={provided.innerRef}>
                {columns &&
                  columns.map((column: any, index: number) => (
                    <ColumnElement key={column.id} column={column} tasks={tasks} index={index} />
                  ))}
                {provided.placeholder}
              </ListGrid>
            )}
          </Droppable>
        )}
      </DragDropContext>
    </DragDropContextContainer>
  )
}

export default DragListModel
